CREATE TABLE Persons (
    PersonID int,
    LastName varchar(20),
    FirstName varchar(20),
    City varchar(20) 
);

INSERT INTO Persons(PersonID, LastName, FirstName, City)
VALUES(1, "Onegin", "Viktor", "Tallinn");