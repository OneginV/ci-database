var mysql = require('mysql');

var con = mysql.createConnection({
  host: "mysql",
  user: "test",
  password: "test",
  database: "test"
});

con.connect(function(err) {
  if (err) throw err;
  con.query("SELECT * FROM Persons", function (err, result, fields) {
    if (err) throw err;
    console.log(result);
  });
});